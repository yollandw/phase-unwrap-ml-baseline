from abc import ABC, abstractclassmethod
import os
import sys
from typing import Callable

import numpy as np
import pandas as pd
import tifffile

from torch.utils.data import Dataset

from utils.utils import shift_to_zero, normalize_gt


def factory(
        ds_config: dict,
        root: str, 
        df: pd.DataFrame,
        transforms: Callable = None,
        label_transforms: Callable = None) -> Dataset:
    try:
        return getattr(sys.modules[__name__], ds_config['type'])(
            root, df, transforms, label_transforms, **(ds_config['params'] if 'params' in ds_config else {}))
    except AttributeError:
        raise ValueError(f'Invalid dataset specified in config: {ds_config["type"]}')


class PuDataset(Dataset, ABC):
    def __init__(self, root, df, transforms, label_transforms):
        self.root = root
        self.df = df
        self.transforms = transforms
        self.label_transforms = label_transforms

    def set_transforms(self, transforms: Callable):
        self.transforms = transforms

    @abstractclassmethod
    def __len__(self):
        pass

    @abstractclassmethod
    def __getitem__(self, ix):
        sample = {
            'image_id': None,
            'wrapped_phase': None,
            'true_phase': None
        }
        pass


class PuSARDataset(PuDataset):
    def __init__(self, root, df, transforms=None, label_transforms=None):
        super(PuSARDataset, self).__init__(root, df, transforms, label_transforms)
        self.label_transforms = normalize_gt

    def __len__(self):
        return len(self.df)

    def __getitem__(self, ix):
        row = self.df.iloc[ix]
        return 


class PuSyntheticSARDataset(PuDataset):
    def __init__(self, root, df, transforms=None, label_transforms=None):
        super(PuSyntheticSARDataset, self).__init__(root, df, transforms, label_transforms)
        self.label_transforms = normalize_gt

    def __len__(self):
        return len(self.df)

    def __getitem__(self, ix):
        row = self.df.iloc[ix]
        image_id = row['image_id']

        wrapped_phase = tifffile.imread(os.path.join(self.root, f"demwrap_{image_id}.tif")).astype('float32')
        true_phase = tifffile.imread(os.path.join(self.root, f"dem_{image_id}.tif"))

        if self.transforms is not None:
            wrapped_phase = self.transforms(wrapped_phase)

        if self.label_transforms is not None:
            true_phase = self.label_transforms(true_phase)

        return {
            'image_id': image_id,
            'wrapped_phase': wrapped_phase,
            'true_phase': true_phase
        }
