import os

from torch.utils.tensorboard import SummaryWriter


class TensorboardWriter:
    def __init__(self, tensorboard_dir, model_name, model_stage, dt_string, checkpoint_dir):
        self.dt_string = dt_string
        self.writer = SummaryWriter(os.path.join(tensorboard_dir, model_name, dt_string, model_stage))
        self.output_dir = os.path.join(checkpoint_dir, model_name, dt_string, model_stage)

    def log_metrics(self, metrics, epoch_idx):
        for k, v in metrics.items():
            self.writer.add_scalar(k, v, epoch_idx)

    def log_image(self, image_tag, image, epoch_idx):
        self.writer.add_image(image_tag, image, epoch_idx)
