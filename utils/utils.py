import os
from typing import Union

import torch
import yaml
import numpy as np

from utils.tensorboard import TensorboardWriter


def save_model(model, save_path):
    if not os.path.exists(os.path.dirname(save_path)):
        os.makedirs(os.path.dirname(save_path))
    torch.save({
        'model_state_dict': model.state_dict()
    }, save_path)


def setup_logger(config, model_name, model_stage, dt_string, checkpoint_dir):
    logger = TensorboardWriter(config['log_dir'], model_name, model_stage, dt_string, checkpoint_dir)
    return logger


def load_config(config_path):
    with open(config_path, 'r') as f:
        config = yaml.load(f, Loader=yaml.Loader)
    return config


def shift_to_zero(label: np.array) -> np.array:
    return label - label.min()


def normalize_gt(label: Union[np.array, torch.Tensor]) -> \
        Union[np.array, torch.Tensor]:
    label -= label.min()
    label /= label.max()
    return label


class EarlyStopping:
    def __init__(self,
                 min_delta=0,
                 patience=5):
        self.min_delta = min_delta
        self.patience = patience
        self.wait = 0

        self.best_value = np.inf

    def check(self, value):
        if (value - self.best_value) < -self.min_delta:
            self.best_value = value
            self.wait = 1
            print('New best value: {}'.format(self.best_value))
        else:
            print('Value {} was not better than {}.'.format(value, self.best_value))
            if self.wait >= self.patience:
                print('Ran out of patience after {} epochs'.format(self.wait))
                return True
            print('Waited {} epochs so far...'.format(self.wait))
            self.wait += 1

        return False
