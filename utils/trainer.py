from collections import defaultdict
import math
import os
from utils.metrics import AverageMeter, align_phase, pu_rsnr, pu_ssim

import numpy as np
from tqdm import tqdm

import torch
import torch.nn as nn
from torch.utils.data import DataLoader
import torch.optim.lr_scheduler as lr_scheduler
from torch.optim.lr_scheduler import ReduceLROnPlateau

from utils.tensorboard import TensorboardWriter
from utils.utils import save_model


class Trainer():
    def __init__(self,
                 model: nn.Module,
                 optimizer: torch.optim.Optimizer,
                 scheduler: lr_scheduler,
                 criterion: torch.nn.Module,
                 config: dict,
                 device='cuda:0',
                 checkpoint_dir='checkpoints'):
        self.model = model
        self.optimizer = optimizer
        self.scheduler = scheduler
        self.criterion = criterion
        self.device = device
        self.checkpoint_dir = checkpoint_dir
        self.config = config

        self.metric_meters = {}
        self._reset_meters('train')
        self._reset_meters('val')

        self.measure_every = 100 if 'measure_every' not in config else config['measure_every']
        self.val_every = 1. if 'val_every' not in config else config['val_every']

    def fit(self,
            epochs: int,
            train_loader: DataLoader,
            logger: TensorboardWriter,
            val_loader: DataLoader = None,
            **kwargs):
        """
        Fit model to training data
        :param epochs: (int) Number of iterations through entire training dataset
        :param train_loader: (DataLoader) PyTorch DataLoader containing training dataset
        :param logger: (TensorboardWriter) Wrapper for tensorboard SummaryWriter class to log model performance
        :param val_loader: (DataLoader) PyTorch DataLoader containing validation dataset
        :param kwargs: Keyword arguments mainly used by subclasses
        :return:
        """
        batches_per_epoch = int(math.ceil(len(train_loader.dataset) / train_loader.batch_size))
        self.val_every = self.val_every if 'val_every' not in kwargs else kwargs['val_every']
        self.val_every = int(math.ceil(self.val_every * batches_per_epoch))
        self.measure_every = max(min(self.measure_every, self.val_every), 1)

        self.model.to(self.device)

        global_iteration = 0
        log_val_step = 0
        best_val_loss = np.Inf

        total_iterations = epochs * batches_per_epoch
        bar = tqdm(range(total_iterations), total=total_iterations, desc='Iterations', leave=True)

        self.model.train()
        for _ in range(epochs):
            plot_batch = np.random.randint(0, batches_per_epoch)
            for b_ix, batch in enumerate(train_loader):
                _, image = self._run_sample(batch, return_image=b_ix == plot_batch)
                if image is not None:
                    logger.log_image('train/wrapped|pred|true', image, global_iteration)

                if global_iteration and (global_iteration % self.measure_every == 0):
                    metrics = self._prepare_metrics('train')
                    logger.log_metrics(metrics, global_iteration)

                if global_iteration and (global_iteration % self.val_every == 0):
                    val_loss = self._validate(val_loader, logger, log_val_step)
                    log_val_step += 1
                    self.scheduler.step(val_loss if isinstance(self.scheduler, ReduceLROnPlateau) else None)

                    if val_loss < best_val_loss:
                        best_val_loss = val_loss
                        save_model(self.model, os.path.join(logger.output_dir, 'best.pth'))
                    save_model(self.model, os.path.join(logger.output_dir, 'last.pth'))

                    self.model.train()

                global_iteration += 1
                bar.update()

    # def predict(self, loader: DataLoader) -> dict:
    #     """
    #     Predict soft labels over the given dataloader
    #     :param loader: (DataLoader) PyTorch DataLoader containing dataset on which to create predictions
    #     :return: DataFrame containing filepaths and predicted probabilities for each class
    #     """
    #     self.model.to(self.device)
    #     self.model.eval()

    #     img_ids = []
    #     pseudo_labels = []

    #     with torch.no_grad():
    #         for _, batch in tqdm(enumerate(loader), leave=True, desc='Labeling'):
    #             images = batch['image'].to(self.device)
    #             pred_logits = self.model(images)
    #             soft_labels = torch.softmax(pred_logits, dim=1)
    #             pseudo_labels.append(soft_labels.cpu().detach().numpy())

    #             ids = batch['img_rel_path']
    #             img_ids.append(ids)

    #     pseudo_labels = np.concatenate(pseudo_labels, axis=0)
    #     img_ids = np.concatenate(img_ids, axis=0)

    #     df = {'filepath': img_ids, 'pseudo_label': pseudo_labels}
    #     return df

    def _validate(self, val_loader, logger: TensorboardWriter, log_val_step: int) -> float:
        """
        Run validation over the entire validation dataset
        :param val_loader: (DataLoader) PyTorch DataLoader containing validation data
        :param logger: (TensorboardWriter) Wrapper for tensorboard SummaryWriter class to log model performance
        :param log_val_step: (int) Current validation step
        :return: (float) Mean validation loss over the validation data
        """
        val_loss = []
        batches_per_epoch = int(math.ceil(len(val_loader.dataset) / val_loader.batch_size))

        self.model.eval()

        with torch.no_grad():
            plot_batch = np.random.randint(0, batches_per_epoch)
            for b_ix, batch in tqdm(enumerate(val_loader), leave=True, desc='Validation'):
                loss, image = self._run_sample(batch, is_training=False, return_image=b_ix == plot_batch)
                if image is not None:
                    logger.log_image('val/wrapped|pred|true', image, log_val_step)
                val_loss.append(loss)

        metrics = self._prepare_metrics('val')
        logger.log_metrics(metrics, log_val_step)

        return np.mean(val_loss)

    def _run_sample(self, sample: dict, is_training=True, return_image=False) -> float:
        """
        Perform a single iteration over the input batch
        :param sample: (dict) A training batch
        :param is_training: (bool) Flag indicating whether to backprop and step optimizers
        :return: (tuple)
            1) Mean batch loss
            2) Array containing batch probabilities
            3) Array containing ground-truth labels
        """
        wrapped_phase = sample['wrapped_phase'].to(self.device)
        true_phase = sample['true_phase'].unsqueeze(1).to(self.device)

        pred_phase = self.model(wrapped_phase)
        loss = self.criterion(pred_phase, true_phase)

        if is_training:
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()

        stage = 'train' if is_training else 'val'
        batch_size = wrapped_phase.size()[0]

        batch_ssim = pu_ssim(pred_phase.detach(), true_phase.detach()).mean().item()
        batch_rsnr = pu_rsnr(pred_phase.detach(), true_phase.detach()).mean().item()

        self.metric_meters[stage]['loss'].update(loss.mean().item(), batch_size)
        self.metric_meters[stage]['ssim'].update(batch_ssim, batch_size)
        self.metric_meters[stage]['rsnr'].update(batch_rsnr, batch_size)

        image = None
        if return_image:
            apred_phase = align_phase(pred_phase[0].detach().cpu(), true_phase[0].detach().cpu())

            sample_image = torch.cat((apred_phase, true_phase[0].detach().cpu()), dim=2)
            # sample_image -= true_phase[0].min().item()
            # sample_image /= (true_phase[0].max().item() - true_phase[0].min().item())

            # image = torch.cat((
            #     (wrapped_phase[0].detach().cpu() - np.pi) / (2 * np.pi), sample_image
            # ), dim=2)

            image = torch.cat((
                wrapped_phase[0].detach().cpu(), sample_image
            ), dim=2)

        return loss.mean().item(), image

    def _reset_meters(self, stage):
        self.metric_meters[stage] = defaultdict(AverageMeter)

    def _prepare_metrics(self, stage: str) -> dict:
        """
        Prepare results to log to tensorboard
        :param stage: (str) Current stage (train/eval)
        :return: dict containing metrics to log
        """
        metrics = {}
        for meter_name, meter in self.metric_meters[stage].items():
            metrics[f'{stage}/{meter_name}'] = meter.avg

        self._reset_meters(stage)

        return metrics