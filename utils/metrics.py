from piqa.ssim import ssim as piqa_ssim
from piqa.utils.functional import gaussian_kernel

import torch
from torch.nn import functional as F


class AverageMeter:
    """Computes and stores the average and current value
       Imported from https://github.com/pytorch/examples/blob/master/imagenet/main.py#L247-L262
    """
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


def align_phase(p_phase: torch.Tensor, t_phase: torch.Tensor) -> torch.Tensor:
    # return p_phase + (t_phase.detach() - p_phase.detach()).mean()
    return p_phase


# Yin et al. 2019 - Temporal phase unwrapping using deep learning.
def pu_rsnr(p_phase: torch.Tensor, t_phase: torch.Tensor) -> torch.Tensor:
    ap_phase = align_phase(p_phase, t_phase)
    return 20. * (t_phase.norm(dim=[2, 3]) / (ap_phase - t_phase).norm(dim=[2, 3])).log10()


# Yin et al. 2019 - Temporal phase unwrapping using deep learning.
def pu_ssim(p_phase: torch.Tensor, t_phase: torch.Tensor) -> torch.Tensor:
    ap_phase = align_phase(p_phase, t_phase)
    if min(p_phase.size()[-2:]) > 256:
        ap_phase = F.adaptive_avg_pool2d(ap_phase, (256, 256))
        t_phase = F.adaptive_avg_pool2d(t_phase, (256, 256))
    ssim, _ = piqa_ssim(ap_phase, t_phase, kernel=gaussian_kernel(11, sigma=1.5).repeat(1, 1, 1).to(ap_phase.device))
    return ssim

