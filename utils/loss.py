import sys

import torch
from torch import nn
from torch.nn.modules import loss as nn_loss

from utils.metrics import align_phase


def factory(loss_config) -> nn_loss._Loss:
    try:
        return getattr(sys.modules[__name__], loss_config['type'])(
            **(loss_config['params'] if 'params' in loss_config else {}))
    except AttributeError:
        raise ValueError(f'Invalid loss specified in config: {loss_config["type"]}')


class PuMSELoss(nn.MSELoss):
    def __init__(self, **kwargs):
        super(PuMSELoss, self).__init__(**kwargs)

    def forward(self, p_phase: torch.Tensor, t_phase: torch.Tensor):
        ap_phase = align_phase(p_phase, t_phase)
        return super(PuMSELoss, self).forward(ap_phase, t_phase)


class PuL1Loss(nn.L1Loss):
    def __init__(self, **kwargs):
        super(PuL1Loss, self).__init__(**kwargs)

    def forward(self, p_phase: torch.Tensor, t_phase: torch.Tensor):
        ap_phase = align_phase(p_phase, t_phase)
        return super(PuL1Loss, self).forward(ap_phase, t_phase)
