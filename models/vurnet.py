# Starting from https://github.com/lyuzinmaxim/VUR-Net/blob/main/VURNet.py, with modifications

import torch
import torch.nn as nn

from models.utils import conv1x1, conv3x3, conv_up

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')


class ResidualBlock1(nn.Module):
    def __init__(self, in_channels, out_channels, stride=1):
        super(ResidualBlock1, self).__init__()

        self.conv1 = conv3x3(in_channels, out_channels, stride)
        self.conv2 = conv3x3(out_channels, out_channels)
        self.conv3 = conv1x1(in_channels, out_channels)

        self.bn1 = nn.BatchNorm2d(out_channels)
        self.bn2 = nn.BatchNorm2d(out_channels)

        self.relu = nn.ReLU(inplace=False)

    def forward(self, x):
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        out += self.conv3(x)
        out = self.relu(out)

        return out


class ResidualBlock2(nn.Module):
    def __init__(self, in_channels, out_channels, stride=1):
        super(ResidualBlock2, self).__init__()

        self.conv1 = conv3x3(in_channels, out_channels, stride)
        self.conv2 = conv3x3(out_channels, out_channels)
        self.conv3 = conv3x3(out_channels, out_channels)
        self.conv4 = conv1x1(in_channels, out_channels)

        self.bn1 = nn.BatchNorm2d(out_channels)
        self.bn2 = nn.BatchNorm2d(out_channels)
        self.bn3 = nn.BatchNorm2d(out_channels)

        self.relu = nn.ReLU(inplace=False)

    def forward(self, x):
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        out += self.conv4(x)
        out = self.relu(out)

        return out


def down_creator(in_channels, out_channels):
    return nn.Sequential(
        nn.MaxPool2d(kernel_size=2, stride=2),
        ResidualBlock2(in_channels, out_channels)
    )


class VURnet(torch.nn.Module):
    def __init__(self):
        super(VURnet, self).__init__()

        self.max_pool_2x2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.block1 = ResidualBlock1(in_channels=1, out_channels=64)
        self.block2 = ResidualBlock1(in_channels=64, out_channels=128)

        self.down1 = down_creator(128, 256)
        self.down2 = down_creator(256, 512)
        self.down3 = down_creator(512, 512)
        self.down4 = down_creator(512, 512)

        self.up1 = conv_up(512, 512)
        self.upblock1 = ResidualBlock2(in_channels=1024, out_channels=512)

        self.up2 = conv_up(512, 512)
        self.upblock2 = ResidualBlock2(in_channels=1024, out_channels=512)

        self.up3 = conv_up(512, 256)
        self.upblock3 = ResidualBlock2(in_channels=512, out_channels=256)

        self.up4 = conv_up(256, 128)
        self.upblock4 = ResidualBlock1(in_channels=256, out_channels=128)

        self.up5 = conv_up(128, 64)
        self.upblock5 = ResidualBlock1(in_channels=128, out_channels=64)

        self.conv = conv3x3(in_channels=64, out_channels=1)

    def forward(self, x):
        # encoder
        x1 = self.block1(x)
        x2 = self.block2(self.max_pool_2x2(x1))

        x3 = self.down1(x2)
        x4 = self.down2(x3)
        x5 = self.down3(x4)
        x6 = self.down4(x5)

        # decoder
        y1 = self.up1(x6)
        y1 = torch.cat([y1, x5], 1)
        y1 = self.upblock1(y1)

        y2 = self.up2(y1)
        y2 = torch.cat([y2, x4], 1)
        y2 = self.upblock2(y2)

        y3 = self.up3(y2)
        y3 = torch.cat([y3, x3], 1)
        y3 = self.upblock3(y3)

        y4 = self.up4(y3)
        y4 = torch.cat([y4, x2], 1)
        y4 = self.upblock4(y4)

        y5 = self.up5(y4)
        y5 = torch.cat([y5, x1], 1)
        y5 = self.upblock5(y5)

        out = self.conv(y5)

        return out


if __name__ == "__main__":
    image = torch.rand((1, 1, 256, 256))
    model = VURnet()
    print(model(image).size())
