from torch import nn

import models

def factory(model_config) -> nn.Module:
    try:
        return getattr(models, model_config['type'])(**(model_config['params'] if 'params'  in model_config else {}))
    except AttributeError:
        raise ValueError(f'Invalid loss specified in config: {model_config["type"]}')