# Starting from https://github.com/lyuzinmaxim/PhUn/blob/main/PhUn.py, with modifications

import torch
import torch.nn as nn

from models.utils import conv1x1, conv3x3

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')


class ResidualBlock(nn.Module):
    def __init__(self, in_channels, out_channels, stride=1):
        super(ResidualBlock, self).__init__()

        self.conv1 = conv3x3(in_channels, out_channels, stride)
        self.conv2 = conv3x3(in_channels, out_channels, stride)
        self.conv3 = conv1x1(in_channels, out_channels, stride)

        self.relu = nn.LeakyReLU(inplace=False)

    def forward(self, x):
        out = self.conv1(x)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.relu(out)

        out += self.conv3(x)

        return out


def residualblock4creator(in_channels, out_channels):
    return nn.Sequential(
        ResidualBlock(in_channels, out_channels),
        ResidualBlock(in_channels, out_channels),
        ResidualBlock(in_channels, out_channels),
        ResidualBlock(in_channels, out_channels)
    )


def up_creator(in_channels, out_channels):
    return nn.Sequential(
        conv3x3(in_channels, in_channels * 2),
        nn.LeakyReLU(inplace=False),
        nn.ConvTranspose2d(
            in_channels=in_channels * 2,
            out_channels=out_channels,
            kernel_size=2,
            stride=2)
    )


class PhUn(torch.nn.Module):
    def __init__(self):
        super(PhUn, self).__init__()

        self.conv1 = conv3x3(1, 4)
        self.conv2 = conv3x3(4, 8)
        self.conv3 = conv3x3(8, 16)
        self.conv4 = conv3x3(16, 32)
        self.conv5 = conv3x3(4, 1)
        self.conv6 = conv1x1(4, 1)

        self.block1 = residualblock4creator(4, 4)
        self.block2 = residualblock4creator(8, 8)
        self.block3 = residualblock4creator(16, 16)
        self.block4 = residualblock4creator(32, 32)

        self.block_up1 = up_creator(32, 16)
        self.block_up2 = up_creator(16, 8)
        self.block_up3 = up_creator(8, 4)

        self.max_pool_2x2 = nn.MaxPool2d(kernel_size=2, stride=2)

    def forward(self, x):
        x = self.conv1(x)
        x = self.block1(x)

        branch = self.conv6(x)

        x = self.conv2(x)
        x = self.max_pool_2x2(x)

        x = self.block2(x)
        x = self.conv3(x)
        x = self.max_pool_2x2(x)

        x = self.block3(x)
        x = self.conv4(x)
        x = self.max_pool_2x2(x)

        x = self.block4(x)

        x = self.block_up1(x)
        x = self.block_up2(x)
        x = self.block_up3(x)

        x = self.conv5(x)

        x = x + branch
        return x


if __name__ == "__main__":
    image = torch.rand((1, 1, 512, 512))
    model = PhUn()
    print(model(image).size())
