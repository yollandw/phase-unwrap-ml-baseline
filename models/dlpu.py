# Starting of https://github.com/lyuzinmaxim/DLPU/blob/main/DLPU.py, with modifications

import torch
import torch.nn as nn

from models.utils import conv3x3, conv_up

device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')


# Taken from torchvision.models.resnet and simplified for usecase
class BasicBlock(nn.Module):
    def __init__(self, in_channels, out_channels, stride=1, groups=1, base_width=64):
        super(BasicBlock, self).__init__()
        if groups != 1 or base_width != 64:
            raise ValueError('BasicBlock only supports groups=1 and base_width=64')

        self.conv1 = conv3x3(in_channels, out_channels, stride)
        self.conv2 = conv3x3(out_channels, out_channels)

        self.bn1 = nn.BatchNorm2d(out_channels)
        self.bn2 = nn.BatchNorm2d(out_channels)

        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        out += x
        out = self.relu(out)

        return out


class ResidualBlockDown(nn.Module):
    def __init__(self, in_channels, out_channels, stride=1):
        super(ResidualBlockDown, self).__init__()

        self.conv1 = conv3x3(in_channels, out_channels, stride)
        self.conv2 = conv3x3(out_channels, out_channels)

        self.bb1 = BasicBlock(out_channels, out_channels)

        self.bn1 = nn.BatchNorm2d(out_channels)
        self.bn2 = nn.BatchNorm2d(out_channels)

        self.relu = nn.ReLU(inplace=False)

    def forward(self, x):
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.bb1(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        return out


class ResidualBlockUp(nn.Module):
    def __init__(self, in_channels, out_channels, stride=1):
        super(ResidualBlockUp, self).__init__()

        self.conv1 = conv3x3(in_channels, in_channels // 2, stride)
        self.conv2 = conv3x3(in_channels // 2, out_channels)

        self.bb1 = BasicBlock(in_channels // 2, in_channels // 2)

        self.bn1 = nn.BatchNorm2d(in_channels // 2)
        self.bn2 = nn.BatchNorm2d(out_channels)

        self.relu = nn.ReLU(inplace=False)

    def forward(self, x):
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.bb1(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        return out


class BottleneckBlock(nn.Module):
    def __init__(self, in_channels, stride=1):
        super(BottleneckBlock, self).__init__()

        self.conv1 = conv3x3(in_channels, 2 * in_channels, stride)
        self.conv2 = conv3x3(2 * in_channels, in_channels)

        self.bb1 = BasicBlock(2 * in_channels, 2 * in_channels)

        self.bn1 = nn.BatchNorm2d(2 * in_channels)
        self.bn2 = nn.BatchNorm2d(in_channels)

        self.relu = nn.ReLU(inplace=False)

    def forward(self, x):
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.bb1(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        return out


class DLPU(torch.nn.Module):
    def __init__(self):
        super(DLPU, self).__init__()

        self.max_pool_2x2 = nn.MaxPool2d(kernel_size=2, stride=2)

        self.block1 = ResidualBlockDown(1, 8)
        self.block2 = ResidualBlockDown(8, 16)
        self.block3 = ResidualBlockDown(16, 32)
        self.block4 = ResidualBlockDown(32, 64)
        self.block5 = ResidualBlockDown(64, 128)

        self.bottleneck1 = BottleneckBlock(128)

        self.block_up1 = ResidualBlockUp(256, 64)
        self.block_up2 = ResidualBlockUp(128, 32)
        self.block_up3 = ResidualBlockUp(64, 16)
        self.block_up4 = ResidualBlockUp(32, 8)
        self.block_up5 = ResidualBlockUp(16, 1)

        self.up_trans_1 = conv_up(128, 128)
        self.up_trans_2 = conv_up(64, 64)
        self.up_trans_3 = conv_up(32, 32)
        self.up_trans_4 = conv_up(16, 16)
        self.up_trans_5 = conv_up(8, 8)

    def forward(self, x):
        # encoder
        x1 = self.block1(x)
        x2 = self.max_pool_2x2(x1)

        x3 = self.block2(x2)
        x4 = self.max_pool_2x2(x3)

        x5 = self.block3(x4)
        x6 = self.max_pool_2x2(x5)

        x7 = self.block4(x6)
        x8 = self.max_pool_2x2(x7)

        x9 = self.block5(x8)
        x10 = self.max_pool_2x2(x9)

        # bottleneck
        x11 = self.bottleneck1(x10)

        # decoder
        out = self.up_trans_1(x11)
        out = torch.cat([out, x9], 1)
        out = self.block_up1(out)

        out = self.up_trans_2(out)
        out = torch.cat([out, x7], 1)
        out = self.block_up2(out)

        out = self.up_trans_3(out)
        out = torch.cat([out, x5], 1)
        out = self.block_up3(out)

        out = self.up_trans_4(out)
        out = torch.cat([out, x3], 1)
        out = self.block_up4(out)

        out = self.up_trans_5(out)
        out = torch.cat([out, x1], 1)
        out = self.block_up5(out)

        return out


if __name__ == "__main__":
    image = torch.rand((1, 1, 256, 256))
    model = DLPU()
    print(model(image).size())
