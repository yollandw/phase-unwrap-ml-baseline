import argparse
from datetime import datetime
import os
import random
import shutil

import pandas as pd
import numpy as np

import torch
from torch import nn
from torch.utils.data import DataLoader

from models.factory import factory as model_factory
from utils.loss import factory as loss_factory
from utils.dataset import factory as dataset_factory
from utils.utils import setup_logger, load_config
from utils.transforms import create_transform
from utils.trainer import Trainer


random.seed(0)
np.random.seed(0)
torch.manual_seed(0)
torch.cuda.manual_seed(0)
torch.backends.cudnn.deterministic = True


parser = argparse.ArgumentParser()
parser.add_argument('--config', type=str, required=True)
parser.add_argument('--checkpoint-dir', type=str, default='checkpoints')
parser.add_argument('--device', type=str, default='cuda')


def build_optimizer(config: dict, model: nn.Module) -> torch.optim.Optimizer:
    return getattr(torch.optim, config['type'])(model.parameters(), **config['params'])


def build_scheduler(config: dict, optimizer: torch.optim.Optimizer) -> torch.optim.lr_scheduler:
    return getattr(torch.optim.lr_scheduler, config['type'])(optimizer, **config['params'])


def build_transforms(config: dict) -> dict:
    return {stage: create_transform(config[stage]) for stage in ['train', 'val']}


def build_trainer(trainer_config: dict, config: dict, device: str) -> Trainer:
    network = model_factory(config['networks'][trainer_config['network']])
    optimizer = build_optimizer(config['optimizers'][trainer_config['optimizer']], network)
    scheduler = build_scheduler(config['schedulers'][trainer_config['scheduler']], optimizer)
    criterion = loss_factory(config['criteria'][trainer_config['criteria']])
    return Trainer(network, optimizer, scheduler, criterion, config, args.device)


def build_datasets(config: dict):
    datasets = {}
    for k, ds_config in config['datasets'].items():
        image_root = ds_config['imroot']
        csv_path = os.path.join(config['data_root'], ds_config['csv'])

        with open(csv_path, 'r') as f:
            df = pd.read_csv(f)

        # Select filtered examples
        dataset = dataset_factory(ds_config, image_root, df, None)

        datasets[k] = dataset

    return datasets


def build_loader(dataset, transforms, batch_size, shuffle, drop_last, num_workers=4):
    dataset.set_transforms(transforms)
    return DataLoader(dataset, batch_size, shuffle, drop_last=drop_last, num_workers=num_workers)


if __name__ == '__main__':
    args = parser.parse_args()
    config = load_config(args.config)

    data_root = config['data_root']
    epochs = config['epochs']

    # Setup tensorboard
    dt_string = datetime.now().strftime('%Y%m%d_%H%M')

    # Copy config file to checkpoints
    model_path = os.path.join(args.checkpoint_dir, config['model_name'], dt_string)
    if not os.path.exists(model_path):
        os.makedirs(model_path)
    shutil.copy(args.config, model_path)

    # Build all datasets
    datasets = build_datasets(config)

    for trainer_name, trainer_config in config['trainers'].items():
        logger = setup_logger(config['logging'], config['model_name'], trainer_name, dt_string, args.checkpoint_dir)
        # Build trainer
        trainer = build_trainer(trainer_config, config, args.device)

        # Build trainer-specific transformations
        transforms = build_transforms(config['transforms'][trainer_config['transforms']])

        # Build dataloaders with trainer-specific transformations
        train_loader = build_loader(
            datasets['train'], transforms['train'], trainer_config['batch_size'], shuffle=True, drop_last=False)
        val_loader = build_loader(
            datasets['val'], transforms['val'], trainer_config['batch_size'], shuffle=False, drop_last=False)

        trainer.fit(config['epochs'], train_loader, logger, val_loader)

        logger.writer.flush()
